import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent} from './home/home.component';
import { PeliculasComponent} from './peliculas/peliculas.component';
import { MovieItemComponent } from './movie-item/movie-item.component';
import { SearchComponent } from './search/search.component';

const routes: Routes = [
{ path: 'home', component: HomeComponent},
{ path: 'peliculas', component: PeliculasComponent },
{ path: 'detail/:id', component: MovieItemComponent},
{ path: 'detail', component: SearchComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
