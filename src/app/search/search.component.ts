import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieService } from '../servicios/movie.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  @Output() query = new EventEmitter();

  constructor(private movieService: MovieService) {}

  ngOnInit() {
  }

  search(form: NgForm) {
    const searchValue = form.value.search;
    if (searchValue) {
      this.query.emit({data: searchValue});
    }
  }
}
