import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { MovieService } from '../servicios/movie.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-movie-item',
  templateUrl: './movie-item.component.html',
  styleUrls: ['./movie-item.component.css']
})

//exporto la clase movieitemcomponent 
export class MovieItemComponent implements OnInit {
  //con esto le digo que me llegara un imput con el nombre de movie que es lo que pongo en
  // peliculas.component.html [movie]="movie" asi iria porque este es un 
  //componente hijo de peliculas.component.html
  @Input() movie: any = {};


  id: string;
  _movie: any;
   
  constructor(private movieService: MovieService,
    private activaterouter: ActivatedRoute,
    private router: Router) {

    this.activaterouter.params.subscribe(params => {
      this.id = params['id'];

      if (this.id){
        this.movieService.getMovieById(this.id).subscribe(
          datos => {
            this.movie = datos;
          });
      }
    })
  }

  ngOnInit() {
  }

goToPeliculas(){
  this.router.navigate(['/peliculas']);

}

}
