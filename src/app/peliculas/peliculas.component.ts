import { Component, OnInit } from '@angular/core';
import { MovieService } from '../servicios/movie.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.css']
})

export class PeliculasComponent implements OnInit {

  movies: any;
  filteredMovies: any;

  constructor(
    private movieService: MovieService,
    private router: Router) { }

  ngOnInit() {
    this.movieService.getPopularMovies(1)
      .subscribe(
        (response: any) => {
          this.movies = [...response.results];
          this.filteredMovies = [...response.results];
        },
        (error) => {
          console.log('error: ', error);
        });
  }

  goToDetail(id) {
    this.router.navigate(['/detail', id]);
    console.log(id);
  }

  onSearchChange(event) {
    console.log('movies', this.movies);
    this.filteredMovies = this.movies.filter((movie: any) => {
      return movie.title.toLowerCase().includes(event.data.toLowerCase());
    }); 
  }
}
