import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

//exporto el servicio
export class MovieService {

  urlMovieDb = 'https://api.themoviedb.org/3';
  apiKey = '0e90da3d1d5b2554b95ae8e121183b3a';
  popu = '/discover/movie?sort_by=popularity.desc&api_key=';
 // searchKeyword = 'search/keyword?api_key=';



  constructor(private http: HttpClient, ) { }
  getPopularMovies(count: Number) {
    const moviedbUrl = `${this.urlMovieDb}${this.popu}${this.apiKey}`;
    return this.http.get<any[]>(moviedbUrl);
  }

  getMovieById(id) {
    const moviedbUrl = `${this.urlMovieDb}/movie/${id}?api_key=${this.apiKey}`;
    return this.http.get<any[]>(moviedbUrl);
  }

  getMovieByKeyWord(keyWord){
    const moviedbUrl = `${this.urlMovieDb}/search/movie?api_key=${this.apiKey}&query=${keyWord}`;
    return this.http.get<any[]>(moviedbUrl);
  }
}

