import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myPipe'
})
export class MyPipePipe implements PipeTransform {

  imagePathUrl = 'https://image.tmdb.org/t/p/w500/';

  transform(value: string): string {
    if (value !== undefined) {
      return this.imagePathUrl + value;
    }
  }
}
